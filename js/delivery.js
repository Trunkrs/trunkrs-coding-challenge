const MATRIX_WIDTH = 1000;
const MATRIX_HEIGHT = 1000;
const MAX_DRIVER_DISTANCE = Math.max(MATRIX_WIDTH, MATRIX_HEIGHT) * 1.5;
const SHIPMENTS = 30;

if (typeof exports !== 'undefined') {
  exports.MATRIX_WIDTH = MATRIX_WIDTH;
  exports.MATRIX_HEIGHT = MATRIX_HEIGHT;
  exports.SHIPMENTS = SHIPMENTS;
}
