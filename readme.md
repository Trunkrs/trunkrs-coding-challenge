# Trunkrs Coding Challenge

## Introduction

This coding challenge is designed to assess your JavaScript and web development skills. It takes place in a fictive world defined by a matrix [1000, 1000]. In this world shipments are created and they hold a status either 'IN_TRANSIT' or 'COMPLETE'. Shipment locations are defined by an x and y variable matching the grid.

## Overview assignment

For this assignment we have already created a simple Node.js back-end. It creates shipments at a random interval and changes the status of shipments to complete, again at a random interval. Your assignment is to create a front-end displaying this data (in whatever way you seem fit) in (near) real time. We would also like to see you group the shipments into routes. Drivers start at <0, 0> (left, top) and can drive a maximum of 1.5 times the largest matrix size (width or height).

## Project structure

* server.js holds the back-end, you will not need to edit this file
* delivery.html is the main html file in which you will display the shipments
* js/delivery.js holds variables shared by the server and front-end (matrix size, no. shipments)

To run the project use `npm start`

## Endpoints

* After running this file `http://localhost:8888` will load the delivery.html file
* `http://localhost:8888/polling` gives a JSON with the current shipments

## So now what?

1. Create a visual front-end to show off the shipments and there status
2. Setup some sort of long polling (Fetch API) to retrieve the shipment information
3. Be sure to use the variables defined in js/delivery.js so the application is flexible
4. Group shipments into routes no, which are no longer than the MAX_DRIVER_DISTANCE
5. Show off your test writing skills
6. Try to avoid JavaScript libraries and frameworks. This is about your skills
7. Try to complete it within 3 hours
8. Make sure it runs in the newest version of the Chrome browser
9. Check how performant your solution is by increasing the number of shipments created

**We love to see:** ES6, promises, web workers, well commented code, well written tests

## Bonus points

For creating the least amount of routes possible and for making it visual attractive
