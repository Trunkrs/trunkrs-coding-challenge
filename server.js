const fs = require('fs');
const http = require('http');
const url = require('url');
const path = require('path');
const util = require('util');
const delivery = require('./js/delivery');
const results = { deliveries: [] };
const dir = path.dirname(fs.realpathSync(__filename));
const INTERVAL = 10;

function addDelivery() {
  if (results.deliveries.length < delivery.SHIPMENTS) {
    const x = Math.round(Math.random() * delivery.MATRIX_WIDTH);
    const y = Math.round(Math.random() * delivery.MATRIX_HEIGHT);
    const rand = (Math.random() * INTERVAL * 4) * 1000;
    results.deliveries.push({
      x,
      y,
      status: 'IN_TRANSIT',
    });
    setTimeout(updateStatus, rand);
  }
}

function updateStatus() {
  const transits = results.deliveries.filter(shipment => shipment.status === 'IN_TRANSIT');
  if (transits.length > 0) {
    const rand = Math.floor(Math.random() * transits.length);
    Object.assign(transits[rand], {
      status: 'COMPLETE',
    });
  }
}

function timer() {
  const rand = (Math.random() * INTERVAL) * 1000;
  setTimeout(addDelivery, rand);
  if (results.deliveries.length < delivery.SHIPMENTS) {
    setTimeout(timer, rand);
  }
}

timer();

http.createServer((req, res) => {
  const pathname = url.parse(req.url).pathname;
  let match;
  if (pathname == '/') {
    res.writeHead(200, {'Content-Type': 'text/html'});
    fs.createReadStream(dir + '/delivery.html').pipe(res);
    return;
  } else if (match = pathname.match(/^\/js\//)) {
    const filename = dir + pathname;
    const stats = fs.existsSync(filename) && fs.statSync(filename);
    if (stats && stats.isFile()) {
      res.writeHead(200, {'Content-Type' : 'application/javascript'});
      fs.createReadStream(filename).pipe(res);
      return;
    }
  } else if (match = pathname.match(/^\/polling/)) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.write(JSON.stringify(results));
    res.end();
    return;
  }
  res.writeHead(404, {'Content-Type': 'text/plain'});
  res.write('404 Not Found\n');
  res.end();
}).listen(8888, 'localhost');

console.log('deliver server running on port 8888');
